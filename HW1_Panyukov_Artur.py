import inspect


def partial(func, *fixated_args, **fixated_kwargs):
    try:
        sig = inspect.signature(func)
    except ValueError:
        sig = None

    def wrapper(*args, **kwargs):
        new_args = fixated_args + args
        new_kwargs = {**kwargs, **fixated_kwargs}
        return func(*new_args, **new_kwargs)

    wrapper.__name__ = 'partial_{func.__name__}'
    if sig is None:
        string = '    ' + str(fixated_args).strip('()') + '\n'
        for key in fixated_kwargs:
            string += f'    {key}={fixated_kwargs[key]}\n'
    else:
        string = ''
        for name in sig.parameters:
            string += f'    {name} = {sig.parameters[name]}\n'
    wrapper.__doc__ = f"""
    A partial implementation of {func.__name__}
    with pre-applied arguements being:\n""" + string
    return wrapper


def test():

    def foo(one, *args, **kwargs):
        pass

    new_round = partial(round, ndigits=2)
    print(round(1.22222))
    print(new_round.__doc__)

    new_foo = partial(foo, 1, node='node')
    print(new_foo.__doc__)

    new_print = partial(print, 1, sep=', ')
    new_print(2, 3, 4, sep='', end=': end.\n')
    new_print()
    print(new_print.__name__)
    print(new_print.__doc__)

    new_max = partial(max, 3)
    print(new_max(1, 2))
    print(new_max.__doc__)


test()
