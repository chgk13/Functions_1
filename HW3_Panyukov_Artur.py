def atom(var=None):

    def get_value():
        nonlocal var
        return var

    def set_value(new_value):
        nonlocal var
        var = new_value
        return var

    def process_value(*funcs):
        nonlocal var
        return [func(var) for func in funcs]

    return get_value, set_value, process_value


get_x, set_x, process_x = atom(2)
print(get_x())
set_x(3)
var = 0
print(get_x())
print(process_x(lambda x: x ** 2, lambda x: x*3, lambda x: x ** 3))
