def letters_range(first, *args):
    letters = []
    for code in range(ord('a'), ord('z')+1):
        letters.append(chr(code))
    len_args = len(args)
    index = letters.index
    if len_args == 0:
        return letters[:index(first)]
    elif len_args == 1:
        return letters[index(first):index(args[0])]
    elif len_args == 2:
        return letters[index(first):index(args[0]):args[1]]
    else:
        raise Exception('Too many arguments!')


def test():
    assert letters_range('b', 'w', 2) == [
        'b', 'd', 'f', 'h', 'j', 'l', 'n', 'p', 'r', 't', 'v']
    assert letters_range('g') == ['a', 'b', 'c', 'd', 'e', 'f']
    assert letters_range('g', 'p') == [
        'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o']
    assert letters_range('p', 'g', -2) == ['p', 'n', 'l', 'j', 'h']
    assert letters_range('a') == []
    print('Testing is done')


test()
