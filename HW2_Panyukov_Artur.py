
def make_it_count(func, counter_name):
    def wrapper(*args, **kwargs):
        globals()[counter_name] += 1
        return func(*args, **kwargs)
    wrapper.__name__ = func.__name__
    wrapper.__doc__ = func.__doc__
    return wrapper


counter = 0
len = make_it_count(len, 'counter')

len({})
len([])
len('len')
print(counter)
print(f'name: {len.__name__}', f'doc: {len.__doc__}', sep='\n')
